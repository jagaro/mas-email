# mas-email

Mastodon posts to readable emails.

![Mastodon Website to Email](example.jpeg)

## Setup

Prerequisites: [git](https://git-scm.com/), [Node >= v14](https://nodejs.org/).

```sh
git clone https://gitlab.com/jagaro/mas-email.git
cd mas-email
npm install
cp .env.example .env
chmod 600 .env
```

Edit `.env`.

If you leave `MAIL_RELAY_HOST` empty, a test account via
[ethereal.email](https://ethereal.email/) will be used.

## Usage

```sh
./mas-email.js
```

Subsequent runs will fetch newer posts.

Write a status ID into `.lastid` (look at the URL of a post) to start fetching
posts from that point:

```sh
echo 110402445686137589 > .lastid
```
