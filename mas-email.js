#!/usr/bin/env node

import { format } from "date-fns";
import "dotenv/config";
import fetch from "node-fetch";
import fs from "fs/promises";
import mjml2html from "mjml";
import nodemailer from "nodemailer";
import path from "path";
import sharp from "sharp";

const PAGE_WIDTH = 600;
const IMAGE_WIDTH = 400;
const LASTID_PATH = path.resolve(".lastid");

const humanToBoolean = (value) =>
  !["", "UNDEFINED", "NAN", "NULL", "N", "NO", "F", "FALSE"].includes(
    String(value).toUpperCase()
  );

const humanToInteger = (value) => {
  value = parseInt(value);
  if (!value || value < 0) {
    value = 0;
  }
  return value;
};

const getConfig = () => {
  const env = process.env;
  return {
    mastodonServer: env.MASTODON_SERVER || "mastodon.social",
    mastodonAccount: env.MASTODON_ACCOUNT || "Gargron",
    fetchLimit: humanToInteger(env.FETCH_LIMIT) || 1,
    useInlineImages: humanToBoolean(env.USE_INLINE_IMAGES),
    relayHost: env.MAIL_RELAY_HOST,
    relayPort: humanToInteger(env.MAIL_RELAY_PORT) || 587,
    relaySecure: humanToBoolean(env.MAIL_RELAY_SECURE),
    relayRequireTLS: humanToBoolean(env.MAIL_RELAY_REQUIRE_TLS),
    relayUser: env.MAIL_RELAY_USER,
    relayPassword: env.MAIL_RELAY_PASSWORD,
    fromAddress: env.MAIL_FROM_ADDRESS || "Example Jane <jane@example.com>",
    toAddress: env.MAIL_TO_ADDRESS || "Example Joe <joe@example.org>",
    ccAddress: env.MAIL_CC_ADDRESS,
  };
};

const getLastId = async () => {
  let lastId;
  try {
    lastId = (await fs.readFile(LASTID_PATH)).toString().replace(/[^0-9]/g, "");
    if (!lastId) {
      lastId = "0";
    }
  } catch {
    lastId = "0";
  }
  await setLastId(lastId);
  return lastId;
};

const setLastId = async (lastId) => {
  await fs.writeFile(LASTID_PATH, lastId + "\n");
};

const fetchMastodonJson = async (path) => {
  const config = getConfig();
  const url = `https://${config.mastodonServer}${path}`;
  console.log(`Fetching ${url}`);
  const result = await fetch(url);
  if (result.status >= 400) {
    throw new Error(`ERROR: Server reported ${result.status}`);
  }
  return await result.json();
};

const getPosts = async (lastStatusId) => {
  const config = getConfig();
  const accountId = (
    await fetchMastodonJson(
      `/api/v1/accounts/lookup?acct=${config.mastodonAccount}`
    )
  ).id;
  return await fetchMastodonJson(
    `/api/v1/accounts/${accountId}/statuses?` +
      `limit=${config.fetchLimit}&min_id=${lastStatusId}`
  );
};

const getBufferFromImageUrl = async (url) => {
  console.log(`Fetching ${url}`);
  const response = await fetch(url);
  const buffer = Buffer.from(await response.arrayBuffer());
  return await sharp(buffer)
    .resize({
      width: IMAGE_WIDTH * 2,
    })
    .toFormat("jpeg")
    .toBuffer();
};

const sendMail = async (options) => {
  const config = getConfig();
  let transportConfig;
  if (config.relayHost) {
    transportConfig = {
      host: config.relayHost,
      port: config.relayPort,
      secure: config.relaySecure,
      requireTLS: config.relayRequireTLS,
      auth: {
        user: config.relayUser,
        pass: config.relayPassword,
      },
    };
  } else {
    console.log(`Creating test account`);
    const account = await nodemailer.createTestAccount();
    transportConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: account.user,
        pass: account.pass,
      },
    };
  }

  console.log(`Sending mail via ${transportConfig.host}`);
  const info = await nodemailer.createTransport(transportConfig).sendMail({
    from: config.fromAddress,
    to: config.toAddress,
    cc: config.ccAddress,
    ...options,
  });

  if (!config.relayHost) {
    console.log("Preview URL: " + nodemailer.getTestMessageUrl(info));
  }
};

(async () => {
  try {
    let lastId = await getLastId();
    const posts = await getPosts(lastId);

    if (posts.length < 1) {
      console.log("No new posts.");
      return;
    }

    const config = getConfig(),
      useInlineImages = config.useInlineImages,
      output = [],
      inlineImageUrls = [];

    for (const post of posts) {
      if (post !== posts[0]) {
        output.push(`<mj-divider border-color="lightgrey" />`);
      }

      const postedAt = new Date(post.created_at),
        postedAtHuman = format(postedAt, "PPP");

      output.push(
        `<mj-text align="right" color="grey">${postedAtHuman}</mj-text>`
      );
      output.push(`<mj-text font-size="14px">${post.content}</mj-text>`);

      const imageUrls = (post.media_attachments || [])
        .map((a) => a.url)
        .concat(post.card && post.card.image ? [post.card.image] : []);

      for (const imageUrl of imageUrls) {
        let url = imageUrl;
        if (useInlineImages) {
          url = `cid:image-${inlineImageUrls.length}`;
          inlineImageUrls.push(imageUrl);
        }
        output.push(`<mj-image width="${IMAGE_WIDTH}" src="${url}" />`);
      }
    }

    const html = mjml2html(
      `<mjml>
         <mj-body width="${PAGE_WIDTH}">
           <mj-section>
             <mj-column>
               ${output.join("\n")}
             </mj-column>
           </mj-section>
         </mj-body>
        </mjml>`,
      {}
    ).html;

    let attachments = null;

    if (useInlineImages) {
      attachments = (
        await Promise.all(
          inlineImageUrls.map(async (url) => getBufferFromImageUrl(url))
        )
      ).map((buffer, i) => ({
        cid: `image-${i}`,
        content: buffer,
        filename: `image-${i}.jpeg`,
      }));
    }

    const accountName = posts[0].account.display_name;
    await sendMail({
      subject: `${accountName}'s latest...`,
      html,
      attachments,
    });

    lastId = posts[0].id;
    setLastId(lastId);
  } catch (err) {
    console.error(err);
  }
})();
